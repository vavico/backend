/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.conexion_docentes.Rest;

import jpa.conexion_docentes.Service.DocenteService;
import jpa.conexion_docentes.modelo.vdocentes_ed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@CrossOrigin //Hago enlace con Angular
@RestController
@RequestMapping(path = "/")//va a escuchar todas las peticiones en la raíz

public class RestDocenteController {
    @Autowired //Para instanciar todas las dependencias
    private DocenteService docenServicio;
    
    @GetMapping(path = "/docentes/listar")//lista los Docentes  
    public Page<vdocentes_ed> ListarDocente(Pageable page) {
        return this.docenServicio.listardocentes(page);
    }
    //public List<vdocentes_ed> ListarDocente() {
      //  return docenServicio.listardocentes();
    //}

    @GetMapping(path = "/filter/{Cedula}")
    public Page<vdocentes_ed> buscar(@PathVariable String texto, Pageable page) {
        return this.docenServicio.BuscarxCedula(texto, page);
    }
    
    //public List<vdocentes_ed> buscarporidInven(@PathVariable String Cedula) {
      //  return docenServicio.BuscarxCedula(Cedula);
    //}
}
