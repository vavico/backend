package jpa.conexion_docentes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConexionDocentesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConexionDocentesApplication.class, args);
	}

}
