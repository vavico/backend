/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jpa.conexion_docentes.Service;


import jpa.conexion_docentes.Repository.IDocenteRepo;
import jpa.conexion_docentes.modelo.vdocentes_ed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class DocenteService {
    @Autowired
    private IDocenteRepo repo;
    
    public Page<vdocentes_ed> listardocentes(Pageable page){
        return repo.listar(page);
    }
    
//    public List<vdocentes_ed> listardocentes() {
  //      return repo.findAll();
    //}
    
    public Page<vdocentes_ed> BuscarxCedula(String codigo,Pageable page){ 
    //public List<vdocentes_ed> BuscarxCedula(String Cedula) {
        return repo.findByidCedula(codigo, page);
    }
    
}
